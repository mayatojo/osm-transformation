# Script name: Download_R_base_script.R
# ______________________________________________________________

# Description:
# This script is used in batch jobs to download the current version
# of the R_Geo_Base_Script.R. After the download, this script
# automatically runs the respective Transformation/Upload script.

# Script order in a typical batch job:
# 1. Run Download_R_base_script.R
# 2. Local Transformation/Upload script is executed.
# 3. Local Transformation/Upload script sources R_Geo_Base_Script.R

# ______________________________________________________________

setwd("/data/")

# 1. Load Packages ####
# ______________________________________________________________
# ______________________________________________________________

# Define needed packages
list_of_packages <- c("aws.s3", "aws.ec2metadata")
new_packages <- list_of_packages[!(list_of_packages %in% 
                                     installed.packages()[,"Package"])]
if(length(new_packages)){install.packages(new_packages)} 
lapply(list_of_packages, require, character.only = TRUE)
rm(list_of_packages, new_packages)



# 2. Archivize old base script ####
# ______________________________________________________________
# ______________________________________________________________

# Check if base script is already existent
if(file.exists("R_Geo_Base_functions.R")){
  timestamp  <- as.character(strftime(Sys.time(), "%Y-%m-%d", tz = "Europe/London"))
  namestring <- paste0("R_Geo_Base_functions_", timestamp, ".R") 
  file.rename("R_Geo_Base_functions.R", namestring)
  print(paste0("Old base script was saved as ", namestring, "."))             
}



# 3. Connect to AWS S3 ####
# ______________________________________________________________
# ______________________________________________________________

# Bucket settings
region <- "eu-west-2"
bucket <- "ev-modeling-platform"
Sys.setenv("AWS_DEFAULT_REGION" = region)

# Load bucket
ev_bucket <- aws.s3::get_bucket(bucket       = bucket,
                                region       = region,
                                max          = Inf, 
                                check_region = TRUE)



# 4. Download new base function script ####
# ______________________________________________________________
# ______________________________________________________________

# Download base script
aws.s3::save_object(object = "8_Scripts/R_Geo_Base_Functions.R",
                    file = "R_Geo_Base_Functions.R",
                    bucket = ev_bucket)

print("New base script downloaded.")



# 5. Run local Transformation/Upload script ####
# ______________________________________________________________
# ______________________________________________________________

# ------------------------------------------------------------------------------------------------------------------------------- Only placeholder: Adjust for each script
script_placeholder <- "/usr/local/src/OSM_Transformation.R"

# This script must be adapted for every batch job
source(script_placeholder)